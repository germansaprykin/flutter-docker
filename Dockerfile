FROM debian:stable-slim
MAINTAINER German Saprykin <german.saprykin@it-nomads.com>

ARG FLUTTER_VERSION

RUN set -x \ 
    && apt-get update \
    && apt-get install -y --no-install-recommends git curl unzip bash ca-certificates \
    && rm -rf /var/lib/apt/lists/*

### Remove after resolving https://github.com/flutter/flutter/issues/8493
RUN set -x \ 
    && apt-get update \
    && apt-get install -y --no-install-recommends libgl1-mesa-dev libglu1-mesa \
    && rm -rf /var/lib/apt/lists/*
###

ENV PATH $PWD/flutter/bin:$PATH
RUN git clone https://github.com/flutter/flutter.git
RUN cd flutter && git checkout tags/$FLUTTER_VERSION
RUN flutter doctor
